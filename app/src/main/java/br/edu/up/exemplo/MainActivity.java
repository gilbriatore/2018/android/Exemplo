package br.edu.up.exemplo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
  }

  int contador = 0;

  char jogadorDaVez = 'o';


  String[][] tabuleiro = new String[3][3];

  public void jogar(View v){
    ImageView btn = (ImageView) v;

    Log.d("Jogo", "View: " + v.getId());
    Log.d("Jogo", "TAG: " + v.getTag());

    if (jogadorDaVez == 'o') {
      btn.setImageResource(R.drawable.x_preto_hdpi);
      jogadorDaVez = 'x';
    } else {
      btn.setImageResource(R.drawable.o_verde_hdpi);
      jogadorDaVez = 'o';
    }
    contador++;
    if (contador == 3){
      ImageView imgGanhador = (ImageView) findViewById(R.id.imgGanhador);
      imgGanhador.setImageResource(R.drawable.risco_vertical_1);
    }
  }
}